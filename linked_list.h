#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#include <stdbool.h>


typedef struct list_node {
	int value;
	struct list_node* next;
} list_node;

list_node* create_list(int number);

list_node* list_add_front(int number, list_node** list_node);

list_node* list_add_back(int number, list_node** head);

list_node* list_node_at(int index, list_node* head);

list_node* list_getLast(list_node* head);

void list_free(list_node* head);

int list_get(int index, list_node* head);

int list_length(list_node* head);

long long list_sum(list_node* head);


#endif

