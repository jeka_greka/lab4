#include <stdio.h>
#include <stdbool.h>

#include "linked_list.h"

list_node* read_input(int count);

void print_nth_element(list_node* list, int index);

list_node* read_input(int count){
	int value;
	char character=' ';
	list_node* list = 0;
	int counter = 0;
	if(count == 0) return list;
	while(counter<count && scanf("%d%c", &value, &character)!=EOF){
		if(list == 0) list = create_list(value);
		else list_add_front(value, &list);
		counter++;
	}
	return list;
}


void print_nth_element(list_node* list, int index){
	int length = list_length(list);
	if(length == 0) {
		printf("List contains no elements\n");
		return;
	}	
	if(index >= length) {
		printf("Your number is out of range\n");
		return;
	}
	int value = list_get(index, list);
	printf("%d ", value);
}


int main(void){
int count;
printf("Write a count of integers:\n");
scanf("%d",&count);
if(count>0) printf("Write your numbers:\n");
list_node* list = read_input(count);
if(list == 0) {
	printf("Invalid input\n");
	return 0;
}
int length = list_length(list);
printf("Your reversed numbers: ");
for (int i = 0; i<length; i++) print_nth_element(list, i);
printf("\nSum of numbers: %llu\n", list_sum(list));
list_free(list);

}
