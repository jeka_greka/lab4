#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include "linked_list.h"


list_node* create_list(int number){
	list_node* new_list = (list_node*) malloc(sizeof(list_node));
	new_list->value = number;
	new_list->next = 0;
	return new_list;
}

list_node* list_add_front(int number, list_node** head){
	list_node* new_head = create_list(number);
	new_head->next = *head;
	*head = new_head;
	return new_head;
	
}

list_node* list_add_back(int number, list_node** head){
	list_node* new_node = create_list(number);
	list_node* last = list_getLast(*head);
	last->next = new_node;
	return new_node;
	
}

list_node* list_node_at(int index, list_node* head){
	list_node* current = head;
	int counter = 0;	
	while(counter <index && current){
		counter++;
		current = current->next;	
	}
	return current;
}

list_node* list_getLast(list_node* head){
	list_node* current = head;
	if(current == 0) return 0;
	while(current->next){
		current = current->next;
	}
	return current;
}

int list_get(int index, list_node* head){
	list_node* current = list_node_at(index, head);
	if(current == 0) return 0;
	return current->value;
}

void list_free(list_node* head){
	list_node* next = head->next;
	while(next){
		free(head);
		head = next;
		next = next->next;
	}
	free(head);

}

int list_length(list_node* head){
	int counter = 0;
	list_node* current = head;
	if(current == 0) return 0;
	while(current){
		counter++;
		current = current->next;
	}
	return counter;
}

long long list_sum(list_node* head){
	list_node* current = head;
	long long sum = 0;
	while(current){
		sum += current->value;
		current = current->next; 
	}
	return sum;
}





